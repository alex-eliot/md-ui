import { describe, it, expect } from "vitest"
import { paramsParser } from "@/core"

describe("params", () => {
  it("should parse params", () => {
    // Arrange
    const params = {
      a: 1,
      b: "2",
      c: [1, 2, 3],
      d: { e: 4, f: 5 },
      e: null,
      f: () => 'abc' /* should be ignored */,
      g: { h: { i: 'abc' } } /* deep objects are never gonna be used probably but let's make sure we handle them at least */,
      j: { k: { l: 'abc', m: 'efg' } }
    }

    // Act
    const result = paramsParser(params)

    // Assert
    expect(result).toBe("a=1&b=2&c[]=1&c[]=2&c[]=3&d[e]=4&d[f]=5&e=null&g[h][i]=abc&j[k][l]=abc&j[k][m]=efg")
  })
})
