import { describe, it, expect } from "vitest"
import mangaSingleFixture from "@test/api/fixtures/manga-single.json"
import type { MangaEntity, EntityResult } from "@/types"
import { getMultipleRelationships, getRelationship, is } from "@/helpers"

const fixture = mangaSingleFixture as EntityResult<MangaEntity>

describe('Helpers', () => {
  it('should find the corresponding relationship', () => {
    // Arrange
    const entity = fixture.data
    const relationship = entity.relationships[0]

    // Act
    const result = getRelationship(entity, relationship.type)
    const results = getMultipleRelationships(entity, relationship.type)

    // Assert
    expect(result).toEqual(relationship)
    expect(results).toEqual([relationship])
  })

  it('should return the correct boolean for entity type', () => {
    // Arrange
    const entity = fixture.data
    const relatioship = entity.relationships[0]

    // Act
    const result = is('manga')(entity)
    const result2 = is('author')(relatioship)

    // Assert
    expect(result).toBe(true)
    expect(result2).toBe(true)
  })
})
