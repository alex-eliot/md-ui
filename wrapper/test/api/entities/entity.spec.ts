import axios, { AxiosError, AxiosHeaders } from "axios"
import { describe, it, expect, vi, beforeEach, afterAll } from "vitest"
import { GenericEntity, wrapQueue } from "@/entities"
import { makeUnknownErrorResult } from "@/common"

enum ResponseHeadersOptions {
  WithoutRequestId = 'withRequestId',
  WithoutResponse = 'withResponse',
}

function createAxiosResponse(data: object) {
  return { data }
}

function createApiError(...options: ResponseHeadersOptions[]) {
  return new AxiosError(
    "test",
    'abcd',
    { headers: new AxiosHeaders() },
    {},
    options.includes(ResponseHeadersOptions.WithoutResponse) ? undefined : {
      data: { result: 'error', errors: [] },
      headers: new AxiosHeaders(options.includes(ResponseHeadersOptions.WithoutRequestId) ? {} : {'X-Request-Id': 'abcd'}),
      status: 503,
      statusText: 'Forbidden',
      config: { headers: new AxiosHeaders(), }
    }
  )
}

function createAxiosApiError(...options: ResponseHeadersOptions[]) {
  return new AxiosError(
    "test",
    'abcd',
    { headers: new AxiosHeaders() },
    {},
    options.includes(ResponseHeadersOptions.WithoutResponse) ? undefined : {
      data: { result: 'error' },
      headers: new AxiosHeaders(options.includes(ResponseHeadersOptions.WithoutRequestId) ? {} : {'X-Request-Id': 'abcd'}),
      status: 503,
      statusText: 'Forbidden',
      config: { headers: new AxiosHeaders(), }
    }
  )
}

function createAxiosNetworkError() {
  return new AxiosError(
    "test",
    'abcd',
    { headers: new AxiosHeaders() },
    {},
    {
      data: {},
      headers: new AxiosHeaders(),
      status: 503,
      statusText: 'Forbidden',
      config: { headers: new AxiosHeaders() }
    }
  )

}

describe("Entity", () => {
  beforeEach(() => {
    vi.clearAllMocks()
  })

  afterAll(() => {
    vi.clearAllMocks()
  })

  it('should return the result of an axios response', async () => {
    // Arrange
    vi.spyOn(axios, 'get')
      .mockResolvedValueOnce(createAxiosResponse({ data: { it: 'works' } }))
      .mockResolvedValueOnce(createAxiosResponse({ data: { it: 'works again' } }))

    // Act
    const result = await GenericEntity.search('manga')
    const result2 = await GenericEntity.getById('manga', 'abcd')

    // Assert
    expect(result).toEqual([{ data: { it: 'works' } }, null])
    expect(result2).toEqual([{ data: { it: 'works again' } }, null])
  })

  it('should throw an error for api error responses', async () => {
    // Arrange
    vi.spyOn(axios, 'get')
      .mockRejectedValueOnce(createApiError())

    // Act
    const result = await GenericEntity.search('manga')

    // Assert
    expect(result).toEqual([null, { result: 'error' , errors: [], xRequestId: 'abcd' }])
  })

  it('should throw an unknown api error when no expected error result form', async () => {
    // Arrange
    vi.spyOn(axios, 'get')
      .mockRejectedValueOnce(createAxiosApiError())
      .mockRejectedValueOnce(createApiError(ResponseHeadersOptions.WithoutRequestId))
      .mockRejectedValueOnce(createApiError(ResponseHeadersOptions.WithoutResponse))

    // Act
    const result = await GenericEntity.search('manga')
    const result2 = await GenericEntity.search('manga')
    const result3 = await GenericEntity.search('manga')

    // Assert
    expect(result).toEqual([null, {
      result: 'error',
      xRequestId: 'abcd',
      errors: [
        {
          context: "local",
          detail: "An error occurred while attempting to fetch data from the API.",
          id: "UnknownApiError",
          status: 503,
          title: "Internal error."
        }
      ],
    }])

    expect(result2).toEqual([null, {
      result: 'error',
      xRequestId: null,
      errors: [
        {
          context: "local",
          detail: "An error occurred while attempting to fetch data from the API.",
          id: "UnknownApiError",
          status: 503,
          title: "Internal error."
        }
      ],
    }])

    expect(result3).toEqual([null, {
      result: 'error',
      xRequestId: null,
      errors: [
        {
          context: "local",
          detail: "An error occurred while attempting to fetch data from the API.",
          id: "UnknownApiError",
          status: 500,
          title: "Internal error."
        }
      ],
    }])
  })

  it('should throw a network error when an AxiosError is thrown', async () => {
    // Arrange
    vi.spyOn(axios, 'get')
      .mockRejectedValueOnce(createAxiosNetworkError())

    // Act
    const result = await GenericEntity.search('manga')

    // Assert
    expect(result).toEqual([null, {
      result: 'error',
      xRequestId: null,
      errors: [
        {
          context: "local",
          detail: "An error occurred while attempting to fetch data from the API.",
          id: "UnknownApiError",
          status: 503,
          title: "Internal error."
        }
      ],
    }])
  })

  it('should throw an unknown network error when no AxiosError is thrown', async () => {
    // Arrange
    vi.spyOn(axios, 'get')
      .mockRejectedValueOnce(new Error('test'))

    // Act
    const result = await GenericEntity.search('manga')

    // Assert
    expect(result).toEqual([null, {
      result: 'error',
      xRequestId: null,
      errors: [
        {
          context: "local",
          detail: "test",
          id: "UnknownKnownError",
          status: 500,
          title: "Internal error."
        }
      ],
    }])
  })

  it('should throw an unknown error when what is thrown is not an error', async () => {
    // Arrange
    vi.spyOn(axios, 'get')
      .mockRejectedValueOnce('test')

    // Act
    const result = await GenericEntity.search('manga')

    // Assert
    expect(result).toEqual([null, {
      result: 'error',
      xRequestId: null,
      errors: [
        {
          context: "local",
          detail: "An error occurred while attempting to fetch data from the API.",
          id: "UnknownError",
          status: 500,
          title: "Internal error."
        }
      ],
    }])
  })
})

describe('wrapQueue', () => {
  it('should return the result of the wrapQueue', async () => {
    // Act
    const result = await wrapQueue(() => 'abc')

    // Assert
    expect(result).toEqual('abc')
  })

  it('shuld return an error if the callback throws', async () => {
    // Act
    const result = await wrapQueue(() => { throw new Error("test") })

    // Assert
    expect(result).toEqual([null, makeUnknownErrorResult("Async task queue did not resolve.")])
  })
})
