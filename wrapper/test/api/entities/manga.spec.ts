import axios from "axios"
import { describe, it, expect, vi, afterAll, beforeEach } from "vitest"
import mangaCollectionFixture from "@test/api/fixtures/manga-collection.json"
import mangaSingleFixture from "@test/api/fixtures/manga-single.json"
import { GenericEntity, Manga } from "@/entities"
import type { MangaEntity } from "@/types"
import type { ResultOrError } from "@/common"
import type { CollectionEntityResult, EntityResult } from "@/types"

const fixtureCollection = [mangaCollectionFixture, null] as ResultOrError<CollectionEntityResult<MangaEntity>>
const fixtureSingle = [mangaSingleFixture, null] as ResultOrError<EntityResult<MangaEntity>>

describe("Manga", () => {
  beforeEach(() => {
    vi.clearAllMocks()
  })

  afterAll(() => {
    vi.clearAllMocks()
  })

  it('should return the entity result', async () => {
    // Arrange
    vi.spyOn(GenericEntity, 'search')
      .mockResolvedValueOnce(fixtureCollection)

    vi.spyOn(GenericEntity, 'getById')
      .mockResolvedValueOnce(fixtureSingle)

    // Act
    const result = await Manga.search()
    const result2 = await Manga.getById('abcd')

    // Assert
    expect(result).toEqual(fixtureCollection)
    expect(result2).toEqual(fixtureSingle)
  })

  it('should return statistics', async () => {
    // Arrange
    vi.spyOn(axios, 'get')
      .mockResolvedValueOnce({ data: { result: 'success', data: { it: 'works' } } })

    // Act
    const result = await Manga.getStats('abcd')

    // Assert
    expect(result).toEqual([{ result: 'success', data: { it: 'works' } }, null])
  })
})
