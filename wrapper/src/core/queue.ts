import PQueue from "p-queue"

export const baseApiQueue = new PQueue({
  intervalCap: 5,
  interval: 1000,
})

export const baseCdnQueue = new PQueue({
  intervalCap: 30,
  interval: 1000,
})
