export function paramsParser(params: Record<string, any>) {
  return Object
    .entries(params)
    .filter(([_, value]) => value !== undefined && typeof value !== 'function')
    .map(([key, value]) => parseParam(key, value))
    .join("&")
}

function parseParam(key: string, value: any) {
  if (Array.isArray(value)) 
    return parseArrayParam(key, value)

  if (isObject(value))
    return parseObjectParam(key, value)

  return `${key}=${value}`
}

function parseArrayParam(key: string, value: any[]) {
  return value.map((v) => `${key}[]=${v}`).join("&")
}

function isObject(value: any): value is object {
  return value !== null && typeof value === 'object'
}

function parseObjectParam(key: string, value: any): string {
  return Object
    .entries(value)
    .map(([k, v]) => {
      if (isObject(v))
        return parseObjectParam(`${key}[${k}]`, v)

      return parseParam(`${key}[${k}]`, v)
    })
    .join("&")
}
