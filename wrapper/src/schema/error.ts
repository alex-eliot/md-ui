import { array, enums, literal, nullable, number, object, string } from "superstruct";

export const ErrorSchema = object({
  result: literal('error'),
  errors: array(object({
    context: nullable(enums(['local', 'remote'])),
    detail: string(),
    id: string(),
    status: number(),
    title: string()
  }))
})
