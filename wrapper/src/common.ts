import { AxiosError, type AxiosResponse } from "axios"
import { assert, is } from "superstruct"
import { ErrorSchema } from "@/schema"
import type { CollectionEntityResult, EntityResult, ErrorResult, PlainResult, SuccessResult } from "@/types"

export type AnyResult<T> = EntityResult<T> | CollectionEntityResult<T> | PlainResult<T>

export type ResultOrError<T extends AnyResult<any>> = [
  T | null,
  ErrorResult | null
]

export type SuccessNoError<T extends AnyResult<any>> = [T, null]
export type ErrorNoSuccess = [null, ErrorResult]

export async function attemptRequest<R extends AnyResult<any>> (
  func: () => Promise<AxiosResponse<R>>
): Promise<ResultOrError<R>> {
  try {
    return await executeRequest(func)
  } catch (error) {
    return makeError(error)
  }
}

async function executeRequest<R extends AnyResult<any>>(func: () => Promise<AxiosResponse<R>>): Promise<SuccessNoError<R>> {
  const resp = await func().then(x => x.data) as SuccessResult<R>
  return [resp, null]
}

function makeError(error: any): ErrorNoSuccess {
  if (isApiError(error) && error.response)
    return [null, makeApiError(error)]

  if (isAxiosError(error))
    return [null, makeUnknownAxiosErrorResult(error)]

  if (isError(error))
    return [null, makeErrorResult(error)]

  return [null, makeUnknownErrorResult()]
}

function isApiError(error: any): error is AxiosError {
  return error instanceof AxiosError &&
    is(error.response?.data, ErrorSchema) &&
    error.response.headers['X-Request-Id']
}

function isAxiosError(error: any): error is AxiosError {
  return error instanceof AxiosError
}

function isError(error: any): error is Error {
  return error instanceof Error
}

export function makeApiError(error: AxiosError): ErrorResult {
  assert(error.response?.data, ErrorSchema)

  return {
    ...error.response.data,
    xRequestId: error.response.headers["X-Request-Id"]!.toString(),
  }
}

export function makeUnknownAxiosErrorResult(error: AxiosError): ErrorResult {
  return {
    result: 'error',
    xRequestId: error.response?.headers["X-Request-Id"]?.toString() ?? null,
    errors: [{
      context: "local",
      detail: "An error occurred while attempting to fetch data from the API.",
      id: "UnknownApiError",
      status: error.response?.status ?? 500,
      title: "Internal error."
    }]
  }
}

export function makeErrorResult(error: Error): ErrorResult {
  return {
    result: 'error',
    xRequestId: null,
    errors: [{
      context: "local",
      detail: error.message,
      id: "UnknownKnownError",
      status: 500,
      title: "Internal error."
    }]
  }
}

export function makeUnknownErrorResult(message?: string): ErrorResult {
  return {
    result: 'error',
    xRequestId: null,
    errors: [{
      context: "local",
      detail: message ?? "An error occurred while attempting to fetch data from the API.",
      id: "UnknownError",
      status: 500,
      title: "Internal error."
    }]
  }
}

/* v8 ignore next 3 */
export const Headers = {
  Bearer: (token?: string) => token ? { Authorization: `Bearer ${token}` } : undefined
}
