import type { LanguageAcronym, Relationship, VersionedEntity } from "@/types";

export interface CoverAttributes {
  volume: string;
  fileName: string;
  description: string;
  locale?: LanguageAcronym | null;
}

export type CoverRelatedEntityType = "manga"

export type CoverEntity<P extends CoverRelatedEntityType | never = never> = VersionedEntity<
  "cover_art",
  CoverAttributes,
  CoverRelatedEntityType,
  P
>

export type CoverRelationship = Relationship<CoverEntity>
