import type {
  ArtistEntity, ArtistRelationship, AuthorEntity, AuthorRelationship,
  ChapterEntity, ChapterRelationship,
  CoverEntity, CoverRelationship,
  MangaEntity, MangaRelationship,
  ScanlationGroupEntity, ScanlationGroupRelationship,
  CreatorEntity, CreatorRelationship, LeaderEntity, LeaderRelationship, MemberEntity, MemberRelationship, UserEntity, UserRelationship
} from "@/types";

export type LanguageAcronym =
  | "ar"
  | "az"
  | "bg"
  | "bn"
  | "ca"
  | "cs"
  | "da"
  | "de"
  | "el"
  | "en"
  | "eo"
  | "es"
  | "es-la"
  | "et"
  | "fa"
  | "fi"
  | "fr"
  | "he"
  | "hi"
  | "hu"
  | "hr"
  | "id"
  | "it"
  | "ja"
  | "ja-ro"
  | "ka"
  | "kk"
  | "ko"
  | "ko-ro"
  | "la"
  | "lt"
  | "mn"
  | "ms"
  | "my"
  | "ne"
  | "nl"
  | "no"
  | "pl"
  | "pt"
  | "pt-br"
  | "ro"
  | "ru"
  | "sk"
  | "sq"
  | "sr"
  | "sv"
  | "ta"
  | "te"
  | "th"
  | "tl"
  | "tr"
  | "uk"
  | "vi"
  | "zh"
  | "zh-hk"
  | "zh-ro"
  | "NULL"
  | "??";

export type LinkAcronym =
  | "al"
  | "ap"
  | "bw"
  | "kt"
  | "mu"
  | "nu"
  | "amz"
  | "ebj"
  | "cdj"
  | "mal"
  | "engtl"
  | "raw"
  | "other";

export type LocalizedString = {[key in LanguageAcronym]?: string}
export type Links = {[key in LinkAcronym]?: string}

export type ResponseType = "entity" | "collection"

export type OkResultType<R extends ResponseType, E, Extra extends object> = (Extra & {
  result: "ok"
  response: R
  data: E;
})

export type ErrorResultType = {
  result: "error"
  errors: {
    id: string
    status: number
    title: string
    detail: string
    context: string
  }[]
}

export type Result<
  R extends ResponseType,
  E,
  Extra extends object = {}
> = OkResultType<R, E, Extra>

export type EntityResult<E> = Result<"entity", E>

export type CollectionEntityResult<E> = Result<"collection", E[], {
  limit: number
  offset: number
  total: number
}>

export type SuccessResult<T extends Result<any, any, any>> = T & { result: 'ok' }

export type ErrorResult = {
  result: "error"
  xRequestId: string | null
  errors: {
    id: string
    status: number
    title: string
    detail: string
    context: 'local' | 'remote' | null
  }[]
}

export type PlainResult<T> = (T & {
  result: "ok"
})

export type ExpandedRelationship<T extends AllRelationships> = T & { attributes: EntityMap[T["type"]]["attributes"] }

export type Entity<
  T extends string,
  A,
  R extends keyof RelationshipMap | never = never,
  P extends R = never
> = {
  id: string
  type: T
  attributes: A
  relationships: (ExpandedRelationship<RelationshipMap[P]> | RelationshipMap[Exclude<R, P>])[]
}

export type VersionedEntity<
  T extends string,
  A,
  R extends keyof RelationshipMap | never = never,
  P extends R = never
> = Entity<T, A & {
  version: number
  createdAt: string
  updatedAt: string
}, R, P>

export type Relationship<E extends Entity<any, any, any, any>> = {
  [K in keyof Omit<E, "relationships" | "attributes">]: E[K]
} & {
  attributes?: E["attributes"]
}

export type AllRelationships =
  | MangaRelationship
  | ChapterRelationship
  | AuthorRelationship
  | ArtistRelationship
  | CreatorRelationship
  | MemberRelationship
  | LeaderRelationship
  | UserRelationship
  | ScanlationGroupRelationship
  | CoverRelationship

export type AllEntities =
  | MangaEntity<never>
  | ChapterEntity<never>
  | AuthorEntity<never>
  | ArtistEntity<never>
  | CreatorEntity<never>
  | MemberEntity<any>
  | LeaderEntity<never>
  | UserEntity<never>
  | ScanlationGroupEntity<never>
  | CoverEntity<never>

export type RelationshipMap = {
  manga: MangaRelationship
  chapter: ChapterRelationship
  author: AuthorRelationship
  artist: ArtistRelationship
  creator: CreatorRelationship
  member: MemberRelationship
  leader: LeaderRelationship
  user: UserRelationship
  scanlation_group: ScanlationGroupRelationship
  cover_art: CoverRelationship
}

export type EntityMap = {
  manga: MangaEntity
  chapter: ChapterEntity
  author: AuthorEntity
  artist: ArtistEntity
  creator: CreatorEntity
  member: MemberEntity
  leader: LeaderEntity
  user: UserEntity
  scanlation_group: ScanlationGroupEntity
  cover_art: CoverEntity
}

export type AllExpandedRelationships = ExpandedRelationship<AllRelationships>

export type ExpandedRelationshipMap = {
  [K in keyof RelationshipMap]: ExpandedRelationship<RelationshipMap[K]>
}

export type SelectEntity<E extends AllEntities, T extends AllEntities["type"]> = E extends { type: T } ? E : never
export type SelectRelationship<R extends AllRelationships, T extends AllEntities["type"]> = R extends { type: T } ? R : never
export type SelectExpandedRelationship<R extends AllExpandedRelationships, T extends AllEntities["type"]> = R extends { type: T } ? R : never

export type PopulateRelationship<
  R extends AllRelationships
> = R & {
  attributes: EntityMap[R["type"]]["attributes"]
};

export type GenericListParams = {
  limit?: number;
  offset?: number;
  total?: number;
}
