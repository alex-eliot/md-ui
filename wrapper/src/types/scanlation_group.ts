import type { LanguageAcronym, Relationship, VersionedEntity } from "@/types";

export type ScanlationGroupAttributes = {
  name: string;
  focusedLanguages: LanguageAcronym[];
  website: string;
  ircServer: string;
  ircChannel: string;
  discord: string;
  contactEmail: string;
  twitter: string;
  mangaUpdates: string;
  description: string;
  inactive: boolean;
  official: boolean;
  publishDelay: string;
  locked: boolean;
  version: number;
  createdAt: string;
  updatedAt: string;
  verified: boolean;
}

export type ScanlationGroupRelatedEntityType =
  | "member"
  | "leader"

export type ScanlationGroupEntity<
  P extends ScanlationGroupRelatedEntityType | never = never
> = VersionedEntity<
  "scanlation_group",
  ScanlationGroupAttributes,
  ScanlationGroupRelatedEntityType,
  P
>

export type ScanlationGroupRelationship = Relationship<ScanlationGroupEntity>
