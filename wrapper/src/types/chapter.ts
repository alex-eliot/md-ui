import type { LanguageAcronym, Relationship, VersionedEntity } from "@/types";

export type ChapterAttributes = {
  title: string | null;
  volume: string | null;
  chapter: string | null;
  translatedLanguage: LanguageAcronym;
  uploader: string;
  externalUrl?: string | null;
  pages: number;
  publishAt: string;
  readableAt: string;
}

export type ChapterRelatedEntityType =
  | "scanlation_group"
  | "user"
  | "manga"

export type ChapterEntity<
  P extends ChapterRelatedEntityType | never = never
> = VersionedEntity<
  "chapter",
  ChapterAttributes,
  ChapterRelatedEntityType,
  P
>

export type ChapterRelationship = Relationship<ChapterEntity>
