import type { Relationship, VersionedEntity } from "@/types";

export type UserRole =
  | "ROLE_BANNED"
  | "ROLE_GUEST"
  | "ROLE_UNVERIFIED"
  | "ROLE_USER"
  | "ROLE_MEMBER"
  | "ROLE_GROUP_MEMBER"
  | "ROLE_MD_AT_HOME"
  | "ROLE_GROUP_LEADER"
  | "ROLE_CONTRIBUTOR"
  | "ROLE_POWER_UPLOADER"
  | "ROLE_VIP"
  | "ROLE_STAFF"
  | "ROLE_DESIGNER"
  | "ROLE_PUBLIC_RELATIONS"
  | "ROLE_FORUM_MODERATOR"
  | "ROLE_GLOBAL_MODERATOR"
  | "ROLE_ADMIN"
  | "ROLE_DEVELOPER"

export type UserAttributes = {
  username: string;
  roles: UserRole[];
}

export type UserRelatedEntityType = "scanlation_group"

export type UserEntity<
  P extends UserRelatedEntityType | never = never
> = VersionedEntity<
  "user",
  UserAttributes,
  UserRelatedEntityType,
  P
>

export type UserRelationship = Relationship<UserEntity>

export type MemberEntity<
  P extends UserRelatedEntityType | never = never
> = VersionedEntity<
  "member",
  UserAttributes,
  UserRelatedEntityType,
  P
>

export type MemberRelationship = Relationship<MemberEntity>

export type LeaderEntity<
  P extends UserRelatedEntityType | never = never
> = VersionedEntity<
  "leader",
  UserAttributes,
  UserRelatedEntityType,
  P
>

export type LeaderRelationship = Relationship<LeaderEntity>

export type CreatorEntity<
  P extends UserRelatedEntityType | never = never
> = VersionedEntity<
  "creator",
  UserAttributes,
  UserRelatedEntityType,
  P
>

export type CreatorRelationship = Relationship<CreatorEntity>
