import type { LocalizedString, Relationship, VersionedEntity } from "@/types";

export interface AuthorAttributes {
  biography: LocalizedString;
  imageUrl: string | null;
  name: string;
  twitter: string;
  pixiv: string;
  melonBook: string;
  fanBox: string;
  booth: string;
  nicoVideo: string;
  skeb: string;
  fantia: string;
  tumblr: string;
  youtube: string;
  website: string;
}

export type AuthorRelatedEntityType = "manga"

export type AuthorEntity<
  P extends AuthorRelatedEntityType | never = never
> = VersionedEntity<
  "author",
  AuthorAttributes,
  AuthorRelatedEntityType,
  P
>
export type AuthorRelationship = Relationship<AuthorEntity>
export type ArtistEntity<
  P extends AuthorRelatedEntityType | never = never
> = VersionedEntity<
  "artist",
  AuthorAttributes,
  AuthorRelatedEntityType,
  P
>
export type ArtistRelationship = Relationship<ArtistEntity>
