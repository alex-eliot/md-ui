import type { Entity, LocalizedString } from "@/types";

export type TagGroup = "format" | "genre" | "content" | "theme"

export type TagEntity = Entity<"tag", {
  name: LocalizedString
  description: LocalizedString
  group: TagGroup
}>
