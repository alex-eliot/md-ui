import type {
  GenericListParams, LanguageAcronym, Links, LocalizedString, Relationship, VersionedEntity,
  TagEntity
} from "@/types";

export type MangaDemographic =
  | "shounen"
  | "seinen"
  | "shoujo"
  | "josei"

export type MangaStatus =
  | "ongoing"
  | "completed"
  | "hiatus"
  | "cancelled"

export type MangaContentRating =
  | "safe"
  | "suggestive"
  | "erotica"
  | "pornographic"

export type MangaState =
  | "draft"
  | "submitted"
  | "rejected"
  | "published"

export type MangaAttributes = {
  title: LocalizedString
  altTitles: LocalizedString[]
  description: LocalizedString
  isLocked: boolean
  links: Links
  originalLanguage: LanguageAcronym
  lastChapter: string | null
  lastVolume: string | null
  publicationDemographic: MangaDemographic | null
  status: MangaStatus
  year: number
  contentRating: MangaContentRating
  chapterNumbersResetOnNewVolume: boolean
  availableTranslatedLanguages: LanguageAcronym[]
  latestUploadedChapter: string
  tags: TagEntity[]
  state: MangaState
}

export type MangaRelatedEntityType =
  | "author"
  | "artist"
  | "cover_art"
  | "creator"

export type MangaEntity<
  P extends MangaRelatedEntityType | never = never
> = VersionedEntity<
  "manga",
  MangaAttributes,
  MangaRelatedEntityType,
  P
>

export type MangaRelationship = Relationship<MangaEntity> & {
  related?: 'adapted_from'
}

export type MangaListParams = GenericListParams & {
  ids?: string[];
  title?: string;

  authors?: string[];
  artists?: string[];
  authorOrArtist?: string;
  year?: number;
  status?: MangaStatus[];
  originalLanguage?: LanguageAcronym[];

  includedTags?: string[];
  includedTagsMode?: "OR" | "AND";
  excludedTags?: string[];
  excludedTagsMode?: "OR" | "AND";

  publicationDemographic?: (MangaDemographic | "none")[];

  createdAtSince?: string;
  updatedAtSince?: string;
  order?: {
    title?: "asc" | "desc";
    year?: "asc" | "desc";
    createdAt?: "asc" | "desc";
    updatedAt?: "asc" | "desc";
    latestUploadedChapter?: "asc" | "desc";
    subscribedCount?: "asc" | "desc";
    relevance?: "asc" | "desc";
    rating?: "asc" | "desc";
  };

  hasAvailableChapters?: boolean;
  availableTranslatedLanguage?: LanguageAcronym[];

  includes?: MangaRelatedEntityType[]
  contentRating?: MangaContentRating[]

  group?: string;
}

export type MangaStatisticsAttributes = {
  comments: {
    repliesCount: number
    threadId: number
  } | null
  follows: number
  rating: {
    bayesian: number
    average: number
    distribution: [number, number, number, number, number, number, number, number, number, number]
  }
}

export type MangaStatistics = Record<string, MangaStatisticsAttributes>
