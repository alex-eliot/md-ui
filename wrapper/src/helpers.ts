import type { AllEntities, AllExpandedRelationships, AllRelationships, Entity, SelectEntity, SelectExpandedRelationship, SelectRelationship, RelationshipMap } from "@/types";

export type GetRelationshipFromEntity<
  E extends Entity<any, any, keyof RelationshipMap>,
  R extends E["relationships"][number]["type"]
> = E["relationships"] extends (infer U)[]
  ? U extends AllRelationships
    ? U["type"] extends R
      ? U
      : never
    : never
  : never

type AnyEntity = AllRelationships | AllEntities | AllExpandedRelationships
  
export type GetCorrespondingType<E extends AnyEntity, T extends AnyEntity["type"]> =
  E extends AllEntities
    ? SelectEntity<E, T>
    : E extends AllRelationships
      ? SelectRelationship<E, T>
      : E extends AllExpandedRelationships
        ? SelectExpandedRelationship<E, T>
        : never

export function is<T extends AnyEntity["type"]>(type: T) {
  return <E extends AnyEntity>(entity: E): entity is GetCorrespondingType<E, T> => entity.type === type
}

export function getRelationship<E extends Entity<any, any, keyof RelationshipMap>, T extends E["relationships"][number]["type"]>(entity: E, type: T) {
  return entity.relationships.find((r) => r.type === type) as GetRelationshipFromEntity<E, T>
}

export function getMultipleRelationships<E extends Entity<any, any, keyof RelationshipMap>, T extends E["relationships"][number]["type"]>(entity: E, type: T) {
  return entity.relationships.filter((r) => r.type === type) as GetRelationshipFromEntity<E, T>[]
}
