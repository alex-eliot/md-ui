import axios from "axios";
import { paramsParser, baseApiQueue } from "@/core";
import { attemptRequest, type ErrorNoSuccess, Headers, makeUnknownErrorResult } from "@/common";
import type { AllEntities, CollectionEntityResult, EntityResult } from "@/types";

const entityPathMap: Record<AllEntities["type"], string> = {
  artist: "/author",
  author: "/author",
  chapter: "/chapter",
  cover_art: "/cover",
  creator: "/user",
  manga: "/manga",
  member: "/user",
  leader: "/user",
  user: "/user",
  scanlation_group: "/group"
}

export async function wrapQueue<T>(func: () => T) {
  const result = await baseApiQueue.add(func)
    .catch(() => null)

  if (!result)
    return [null, makeUnknownErrorResult("Async task queue did not resolve.")] as ErrorNoSuccess

  return result
}

export class GenericEntity {
  static async search<T extends AllEntities>(entityType: T["type"], params: any = {}, auth?: string) {
    const url = `${entityPathMap[entityType]}?${paramsParser(params)}`

    return wrapQueue(() => attemptRequest(
      () => axios.get<CollectionEntityResult<T>>(url, { headers: Headers.Bearer(auth) })
    ))
  }

  static async getById<T extends AllEntities>(entityType: T["type"], id: string, params: any = {}, auth?: string) {
    const url = `${entityPathMap[entityType]}/${id}?${paramsParser(params)}`

    return wrapQueue(() => attemptRequest(
      () => axios.get<EntityResult<T>>(url, { headers: Headers.Bearer(auth) })
    ))
  }
}