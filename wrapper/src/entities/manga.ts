import axios from "axios"
import { attemptRequest } from "@/common"
import { GenericEntity, wrapQueue } from "@/entities"
import type { MangaEntity, MangaListParams, MangaRelatedEntityType, MangaStatistics } from "@/types"
import type { PlainResult } from "@/types"

export class Manga {
  static async search<T extends MangaListParams>(params?: T, auth?: string) {
    type Includes = T["includes"]
    type EntitiesIncluded = Includes extends any[] ? Includes[number] : never

    return await GenericEntity.search<MangaEntity<EntitiesIncluded>>("manga", params, auth)
  }

  static async getById<T extends MangaRelatedEntityType>(id: string, includes: T[] = [], auth?: string) {
    return await GenericEntity.getById<MangaEntity<T>>("manga", id, { includes }, auth)
  }

  static async getStats(id: string, auth?: string) {
    return await wrapQueue(() => attemptRequest(
      () => axios.get<PlainResult<{ statistics: MangaStatistics }>>(`/statistics/manga/${id}`, { headers: { Authorization: `Bearer ${auth}` } })
    ))
  }
}
