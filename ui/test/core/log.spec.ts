import { describe, expect, it, vi, afterAll, beforeEach } from "vitest"
import { LogLevel, LogTracker, Logger } from "@/core"

describe("Logger", () => {
  beforeEach(() => {
    vi.restoreAllMocks()
  })

  afterAll(() => {
    vi.restoreAllMocks()
  })

  it("should log messages", () => {
    const tracker = new LogTracker()

    const logger = tracker.getLogger("test")
    logger.log("test")
    expect(logger.records).toEqual(expect.arrayContaining([
      expect.stringContaining("loglevel:LOG"),
      expect.stringContaining("test")
    ]))
  })

  it("should log errors", () => {
    const tracker = new LogTracker()

    const logger = tracker.getLogger("test")
    logger.error("test")
    expect(logger.records).toEqual(expect.arrayContaining([
      expect.stringContaining("loglevel:ERROR"),
      expect.stringContaining("test")
    ]))
  })

  it("should log warnings", () => {
    const tracker = new LogTracker()

    const logger = tracker.getLogger("test")
    logger.warn("test")
    expect(logger.records).toEqual(expect.arrayContaining([
      expect.stringContaining("loglevel:WARN"),
      expect.stringContaining("test")
    ]))
  })

  it("should log info", () => {
    const tracker = new LogTracker()

    const logger = tracker.getLogger("test")
    logger.info("test")
    expect(logger.records).toEqual(expect.arrayContaining([
      expect.stringContaining("loglevel:INFO"),
      expect.stringContaining("test")
    ]))
  })

  it("should log debug", () => {
    const tracker = new LogTracker()

    const logger = tracker.getLogger("test")
    logger.debug("test")
    expect(logger.records).toEqual(expect.arrayContaining([
      expect.stringContaining("loglevel:DEBUG"),
      expect.stringContaining("test")
    ]))
  })

  it("should clear logs", () => {
    const tracker = new LogTracker()

    const logger = tracker.getLogger("test")
    logger.log("test")
    logger.clear()
    expect(logger.records).toEqual([])
  })

  it("should get records", () => {
    const tracker = new LogTracker()
    const logger = tracker.getLogger("test")
    logger.log("test")
    expect(logger.getRecords(LogLevel.LOG)).toEqual(expect.arrayContaining([expect.stringContaining("test")]))
  })

  it("should get records for multiple levels", () => {
    const tracker = new LogTracker()
    const logger = tracker.getLogger("test")
    logger.log("test")
    logger.error("test")
    logger.warn("test")
    logger.info("test")
    logger.debug("test")

    const levels = [
      LogLevel.LOG,
      LogLevel.ERROR,
      LogLevel.INFO,
      LogLevel.DEBUG
    ]

    expect(logger.getRecords(...levels)).toEqual(
      expect.arrayContaining([
        expect.stringContaining("test"),
        expect.not.stringContaining("WARN"),
        expect.stringContaining("LOG"),
        expect.stringContaining("ERROR"),
        expect.stringContaining("INFO"),
        expect.stringContaining("DEBUG")
      ])
    )
  })

  it('should emit logs to console', () => {
    vi.spyOn(Logger, 'getCurrentDateString')
      .mockReturnValue('2021-01-01T00:00:00.000Z')

    const spy = vi
      .spyOn(Logger.prototype, 'logToConsole')
      .mockImplementation(() => {})

    const tracker = new LogTracker()
    const logger = tracker.getLogger("test", [
      LogLevel.LOG,
      LogLevel.ERROR,
      LogLevel.WARN,
      LogLevel.INFO,
      LogLevel.DEBUG
    ])
    logger.log("test")
    logger.error("test")
    logger.warn("test")
    logger.info("test")
    logger.debug("test")

    expect(spy).toHaveBeenCalledWith(expect.stringContaining("test"), LogLevel.LOG)
    expect(spy).toHaveBeenCalledWith(expect.stringContaining("test"), LogLevel.ERROR)
    expect(spy).toHaveBeenCalledWith(expect.stringContaining("test"), LogLevel.WARN)
    expect(spy).toHaveBeenCalledWith(expect.stringContaining("test"), LogLevel.INFO)
    expect(spy).toHaveBeenCalledWith(expect.stringContaining("test"), LogLevel.DEBUG)
  })
})

describe("getLogger", () => {
  beforeEach(() => {
    vi.restoreAllMocks()
  })

  afterAll(() => {
    vi.restoreAllMocks()
  })

  it("should return the same logger for the same name", () => {
    const tracker = new LogTracker()
    
    const logger1 = tracker.getLogger("test")
    const logger2 = tracker.getLogger("test")
    expect(logger1).toBe(logger2)
  })

  it("should return different loggers for different names", () => {
    const tracker = new LogTracker()

    const logger1 = tracker.getLogger("test1")
    const logger2 = tracker.getLogger("test2")
    expect(logger1).not.toBe(logger2)
  })
})

describe("dumpLogs", () => {
  beforeEach(() => {
    vi.restoreAllMocks()
  })

  afterAll(() => {
    vi.restoreAllMocks()
  })

  it("should return all logs", () => {
    const tracker = new LogTracker()

    const logger1 = tracker.getLogger("test1")
    const logger2 = tracker.getLogger("test2")

    logger1.log("test1")
    logger2.log("test2")

    expect(tracker.dumpLogs(LogLevel.LOG)).toEqual(
      expect.arrayContaining([
        expect.stringContaining("test1"),
        expect.stringContaining("test2")
      ])
    )
  })

  it("should return logs for specific levels", () => {
    const tracker = new LogTracker()

    const logger1 = tracker.getLogger("test1")
    const logger2 = tracker.getLogger("test2")

    logger1.log("test1")
    logger2.error("test2")

    expect(tracker.dumpLogs(LogLevel.LOG)).toEqual(
      expect.arrayContaining([
        expect.stringContaining("test1"),
        expect.not.stringContaining("test2")
      ])
    )

    expect(tracker.dumpLogs(LogLevel.ERROR)).toEqual(
      expect.arrayContaining([
        expect.not.stringContaining("test1"),
        expect.stringContaining("test2")
      ])
    )
  })

  it('should return logs in ascending order based on date', () => {
    vi.spyOn(Logger, 'getCurrentDateString')
      .mockReturnValueOnce('2021-01-01T00:00:00.000Z')
      .mockReturnValueOnce('2021-01-01T00:00:00.100Z')
      .mockReturnValueOnce('2021-01-01T00:00:00.200Z')

    const tracker = new LogTracker()

    const logger1 = tracker.getLogger("test1")
    const logger2 = tracker.getLogger("test2")

    logger1.log("test1")
    logger2.log("test2")
    logger1.log("test3")

    const records = tracker.dumpLogs(LogLevel.LOG)

    expect(records[0]).toContain("test1")
    expect(records[1]).toContain("test2")
    expect(records[2]).toContain("test3")
  })
})
