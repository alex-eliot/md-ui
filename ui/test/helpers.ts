import { Context } from "@/core";

export function createContext() {
  return new Context({
    baseUrl: "http://localhost:3000",
    forumUrl: "http://localhost:3000/forum",
    apiUrl: "http://localhost:3000/api",
    cdnUrl: "http://localhost:3000/cdn",
    oAuthUrl: "http://localhost:3000/oauth",
    oAuthClientId: "1234567890"
  });
}

export function getFakeStorage(): Storage {
  return {
    getItem: () => null,
    setItem: () => null,
    removeItem: () => null,
    clear: () => null,
    key: () => null,
    length: 0
  };
}
