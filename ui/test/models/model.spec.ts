import { describe, it, expect } from "vitest"
import { AsyncModel, decorators } from "@/models"
import { createContext, getFakeStorage } from "@test/helpers"
import { AuthContext, Context } from "@/core"
import { WebStorageStateStore } from "oidc-client-ts"

describe("AsyncModel", () => {
  const context = createContext()
  const auth = new AuthContext(context, new WebStorageStateStore({ store: getFakeStorage() }))

  it("should have isFetching and error attributes", () => {
    const model = new AsyncModel(context, auth)
    expect(model.isFetching).toBe(false)
    expect(model.error).toBe(null)
  })

  it("should set isFetching to true while fetching", async () => {
    class Model extends AsyncModel<[]> {
      constructor(public context: Context, public auth: AuthContext) {
        super(context, auth)
      }

      @decorators.isFetching
      async fetch() {
        expect(this.isFetching).toBe(true)
      }
    }

    const model = new Model(context, auth)
    await model.fetch()
    expect(model.isFetching).toBe(false)
  })
})
