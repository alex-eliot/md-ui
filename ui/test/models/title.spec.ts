import { describe, it, expect, vi, afterAll, beforeEach } from "vitest"
import { Manga, MangaEntity, MangaStatistics } from "@mangadex/wrapper"
import { TitleModel, TitleModelData } from "@/models"
import { AuthContext } from "@/core"
import mangaSingle from "@test/fixtures/manga-single.json"
import { createContext, getFakeStorage } from "@test/helpers"
import { WebStorageStateStore, UserManager } from "oidc-client-ts"

const mangaEntity = mangaSingle as MangaEntity<any>

const fakeMangaStats: MangaStatistics = {
  'd4c40e73-251a-4bcb-a5a6-1edeec1e00e7': {
    follows: 123,
    comments: {
      repliesCount: 456,
      threadId: 123
    },
    rating: {
      distribution: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      bayesian: 7.5,
      average: 7.5
    }
  }
}

describe("TitleModel", () => {
  afterAll(() => {
    vi.restoreAllMocks()
  })

  beforeEach(() => {
    vi.restoreAllMocks()
  })

  const context = createContext()
  const auth = new AuthContext(context, new WebStorageStateStore({ store: getFakeStorage() }))

  it('should fetch and populate the title', async () => {
    vi.spyOn(Manga, 'getById').mockResolvedValue([{ data: mangaEntity, response: 'entity', result: 'ok' }, null])
    vi.spyOn(Manga, 'getStats').mockResolvedValue([{ result: 'ok', statistics: fakeMangaStats }, null])
    vi.spyOn(UserManager.prototype, 'getUser').mockResolvedValue({ access_token: undefined } as any)

    const model = new TitleModel(context, auth)
    await model.fetch('1234')
    expect(model.title).toEqual(mangaEntity)
    expect(Manga.getById).toHaveBeenCalledWith('1234', ['artist', 'author', 'cover_art'], undefined)
  })

  it('should fetch with bearer token', async () => {
    vi.spyOn(Manga, 'getById').mockResolvedValue([{ data: mangaEntity, response: 'entity', result: 'ok' }, null])
    vi.spyOn(Manga, 'getStats').mockResolvedValue([{ result: 'ok', statistics: fakeMangaStats }, null])
    vi.spyOn(UserManager.prototype, 'getUser').mockResolvedValue({ access_token: 'fake-token' } as any)

    const model = new TitleModel(context, auth)
    await model.fetch('1234')
    expect(Manga.getById).toHaveBeenCalledWith('1234', ['artist', 'author', 'cover_art'], 'fake-token')
  })

  it('should process the title with the given language', async () => {
    vi.spyOn(Manga, 'getById').mockResolvedValue([{ data: mangaEntity, response: 'entity', result: 'ok' }, null])
    vi.spyOn(Manga, 'getStats').mockResolvedValue([{ result: 'ok', statistics: fakeMangaStats }, null])
    vi.spyOn(UserManager.prototype, 'getUser').mockResolvedValue({ access_token: undefined } as any)

    const model = new TitleModel(context, auth)
    await model.fetch('d4c40e73-251a-4bcb-a5a6-1edeec1e00e7')

    const processedDataEn = model.process('en')
    expect(processedDataEn).toEqual({
      title: 'Seijo-sama? Iie, Toorisugari no Mamonotsukai Desu! ~Zettai Muteki no Seijo wa Mofumofu to Tabi wo Suru~',
      altTitle: '聖女さま？ いいえ、通りすがりの魔物使いです！ ～絶対無敵の聖女はモフモフと旅をする～',
      otherAltTitles: [
        { language: 'en', title: 'Saint? No, Just a Passing Monster Tamer! ~The Completely Unparalleled Saint Travels with Fluffies~' },
        { language: "fr", title: "Une sainte ? Non! Je ne suis qu’une dompteuse de monstres qui ne fait que passer!" },
        { language: "en", title: "Saint? No! I'm Just a Passing Beast Tamer! ~The Invincible Saint and the Quest for Fluff~" }
      ],
      description: "**Synopsis from Kadokawa:**  \nCheats for fluff! The reincarnated Saint doesn't care about dignity!  \nKanata is a girl known as the reborn \"Saint\". With such talent, the profession she chose is… the weakest \"monster tamer\"!?  \nKanata, who finished her previous life alone in a hospital room, only wants to adore and become friends with fluffies all over the world! If there's a dragon aiming for fluffies in the east, it'll be punished with powerful magic, and if there's a foul smell in the west, the entire city will be purified so the fur won't smell!  \nWhen I used my cheats for fluffies, I was worshipped as a Saint by the people I accidentally saved!?  \n\n---\n\n**Synopsis from Niconico Seiga:**  \n\"Finally, finally… I can touch fluffies!\"  \nKanata is a girl known as the reborn \"Saint\". With such talent, the profession she chose is… the weakest \"monster tamer\"!?  \nThat's right, Kanata, who reincarnated from a previous life of loneliness, has chosen to use her cheats to pat all the world's fluffies. All you have to do is adore them to the fullest!!  \nA fast-paced girl's otherworldly adventure story, comicalized! ☆  \n\n---\n\n**Links:**\n[Web Novel (Raw)](https://ncode.syosetu.com/n5694fk/)  \n[Alternative Raw (Comic-Walker)](https://comic-walker.com/contents/detail/KDCW_AM00000022010000_68/)\n[Official Light Novel English (J-Novel Club)](https://j-novel.club/series/saint-no-i-m-just-a-passing-beast-tamer)",
      authors: ['Inumajin'],
      artists: ['Iida Toi'],
      tags: [
        { name: 'Reincarnation', link: 'http://localhost:3000/tag/0bc90acb-ccc1-44ca-a34a-b9f3a73259d0/reincarnation' },
        { name: 'Animals', link: 'http://localhost:3000/tag/3de8c75d-8ee3-48ff-98ee-e20a65c86451/animals' },
        { name: 'Comedy', link: 'http://localhost:3000/tag/4d32cc48-9f00-4cca-9b5a-a839f0764984/comedy' },
        { name: 'Adventure', link: 'http://localhost:3000/tag/87cc87cd-a395-47af-b27a-93258283bbc6/adventure' },
        { name: 'Magic', link: 'http://localhost:3000/tag/a1f53773-c69a-4ce5-8cab-fffcd90b1565/magic' },
        { name: 'Isekai', link: 'http://localhost:3000/tag/ace04997-f6bd-436e-b261-779182193d3d/isekai' },
        { name: 'Adaptation', link: 'http://localhost:3000/tag/f4122d1c-3b44-44d0-9936-ff7502c39ad3/adaptation' },
      ],
      coverUrl: "http://localhost:3000/cdn/covers/d4c40e73-251a-4bcb-a5a6-1edeec1e00e7/072834ae-5183-4ca8-bb25-ec597c340983.jpg.512.jpg",
      statistics: {
        comments: {
          total: 456,
          forumUrl: 'http://localhost:3000/forum/threads/123'
        },
        follows: 123,
        rating: {
          average: 7.5,
          bayesian: 7.5,
          votes: {
            "1": 1,
            "2": 2,
            "3": 3,
            "4": 4,
            "5": 5,
            "6": 6,
            "7": 7,
            "8": 8,
            "9": 9,
            "10": 10
          }
        }
      }
    } satisfies TitleModelData)

    const processedDataFr = model.process('fr')
    expect(processedDataFr).toEqual({
      title: 'Seijo-sama? Iie, Toorisugari no Mamonotsukai Desu! ~Zettai Muteki no Seijo wa Mofumofu to Tabi wo Suru~',
      altTitle: '聖女さま？ いいえ、通りすがりの魔物使いです！ ～絶対無敵の聖女はモフモフと旅をする～',
      otherAltTitles: [
        { language: 'en', title: 'Saint? No, Just a Passing Monster Tamer! ~The Completely Unparalleled Saint Travels with Fluffies~' },
        { language: "fr", title: "Une sainte ? Non! Je ne suis qu’une dompteuse de monstres qui ne fait que passer!" },
        { language: "en", title: "Saint? No! I'm Just a Passing Beast Tamer! ~The Invincible Saint and the Quest for Fluff~" }
      ],
      description: "**Synopsis de Kadokawa:**  \nDes triches (cheats) pour des peluches ! La Sainte réincarnée se fiche de la dignité !Kanata est une fille connue en tant que la « Sainte » ressucitée. Avec un tel don, le travail qu'elle choisie est... le plus faible « Dompteuse de monstres »!?Kanata, qui a fini sa vie précédente toute seule dans une chambre d'hôpital, veut seulement adorer er devenir amis avec des animaux tout doux dans le monde entier! S'il y a un dragon qui s'en prend à ces animaux dans l'est, il sera puni par une magie puissante, et s'il y a une odeur infecte dans l'ouestn la ville entière sera purifiée pour que la fourrure ne sente pas !Quand j'utilisais mes triches (cheats) pour les animaux tout doux, je fus vénérée en tant que Sainte par la population que j'ai accidentellement sauvée !?\n\n---\n\n**Synopsis de Niconico Seiga:**\n\n« Enfin, enfin… Je peux toucher des animaux tout doux ! » Kanata est une fille connue en tant que la « Sainte » ressucitée. Avec un tel don, le travail qu'elle choisie est... le plus faible « Dompteuse de monstres »!?C'est ça, kanata, qui fut réincarnée de sa vie précédente pleine de solitude, a choisi d'utiliser ses triches (cheats) pour carresser tous les animaux tout doux sur Terre. Tout ce que vous avez à faire est de les adorer un maximum !!L'histoire comique des aventures d'une fille venant d'autre part ! ☆\n\n---\n*Traduction par Dafako*\n\n---\n\n**Liens:**\n[Roman original (Raw)](https://ncode.syosetu.com/n5694fk/)\n  [Raw Alternative (Comic-Walker)](https://comic-walker.com/contents/detail/KDCW_AM00000022010000_68/)\n[Roman Officiel Anglais (J-Novel Club)](https://j-novel.club/series/saint-no-i-m-just-a-passing-beast-tamer)",
      authors: ['Inumajin'],
      artists: ['Iida Toi'],
      tags: [
        { name: 'Reincarnation', link: 'http://localhost:3000/tag/0bc90acb-ccc1-44ca-a34a-b9f3a73259d0/reincarnation' },
        { name: 'Animals', link: 'http://localhost:3000/tag/3de8c75d-8ee3-48ff-98ee-e20a65c86451/animals' },
        { name: 'Comedy', link: 'http://localhost:3000/tag/4d32cc48-9f00-4cca-9b5a-a839f0764984/comedy' },
        { name: 'Adventure', link: 'http://localhost:3000/tag/87cc87cd-a395-47af-b27a-93258283bbc6/adventure' },
        { name: 'Magic', link: 'http://localhost:3000/tag/a1f53773-c69a-4ce5-8cab-fffcd90b1565/magic' },
        { name: 'Isekai', link: 'http://localhost:3000/tag/ace04997-f6bd-436e-b261-779182193d3d/isekai' },
        { name: 'Adaptation', link: 'http://localhost:3000/tag/f4122d1c-3b44-44d0-9936-ff7502c39ad3/adaptation' },
      ],
      coverUrl: "http://localhost:3000/cdn/covers/d4c40e73-251a-4bcb-a5a6-1edeec1e00e7/072834ae-5183-4ca8-bb25-ec597c340983.jpg.512.jpg",
      statistics: {
        comments: {
          total: 456,
          forumUrl: 'http://localhost:3000/forum/threads/123'
        },
        follows: 123,
        rating: {
          average: 7.5,
          bayesian: 7.5,
          votes: {
            "1": 1,
            "2": 2,
            "3": 3,
            "4": 4,
            "5": 5,
            "6": 6,
            "7": 7,
            "8": 8,
            "9": 9,
            "10": 10
          }
        }
      }
    } satisfies TitleModelData)

    // Should falback to en
    const processedDataJa = model.process('ja')
    expect(processedDataJa).toEqual({
      title: 'Seijo-sama? Iie, Toorisugari no Mamonotsukai Desu! ~Zettai Muteki no Seijo wa Mofumofu to Tabi wo Suru~',
      altTitle: '聖女さま？ いいえ、通りすがりの魔物使いです！ ～絶対無敵の聖女はモフモフと旅をする～',
      otherAltTitles: [
        { language: 'en', title: 'Saint? No, Just a Passing Monster Tamer! ~The Completely Unparalleled Saint Travels with Fluffies~' },
        { language: "fr", title: "Une sainte ? Non! Je ne suis qu’une dompteuse de monstres qui ne fait que passer!" },
        { language: "en", title: "Saint? No! I'm Just a Passing Beast Tamer! ~The Invincible Saint and the Quest for Fluff~" }
      ],
      description: "**Synopsis from Kadokawa:**  \nCheats for fluff! The reincarnated Saint doesn't care about dignity!  \nKanata is a girl known as the reborn \"Saint\". With such talent, the profession she chose is… the weakest \"monster tamer\"!?  \nKanata, who finished her previous life alone in a hospital room, only wants to adore and become friends with fluffies all over the world! If there's a dragon aiming for fluffies in the east, it'll be punished with powerful magic, and if there's a foul smell in the west, the entire city will be purified so the fur won't smell!  \nWhen I used my cheats for fluffies, I was worshipped as a Saint by the people I accidentally saved!?  \n\n---\n\n**Synopsis from Niconico Seiga:**  \n\"Finally, finally… I can touch fluffies!\"  \nKanata is a girl known as the reborn \"Saint\". With such talent, the profession she chose is… the weakest \"monster tamer\"!?  \nThat's right, Kanata, who reincarnated from a previous life of loneliness, has chosen to use her cheats to pat all the world's fluffies. All you have to do is adore them to the fullest!!  \nA fast-paced girl's otherworldly adventure story, comicalized! ☆  \n\n---\n\n**Links:**\n[Web Novel (Raw)](https://ncode.syosetu.com/n5694fk/)  \n[Alternative Raw (Comic-Walker)](https://comic-walker.com/contents/detail/KDCW_AM00000022010000_68/)\n[Official Light Novel English (J-Novel Club)](https://j-novel.club/series/saint-no-i-m-just-a-passing-beast-tamer)",
      authors: ['Inumajin'],
      artists: ['Iida Toi'],
      tags: [
        { name: 'Reincarnation', link: 'http://localhost:3000/tag/0bc90acb-ccc1-44ca-a34a-b9f3a73259d0/reincarnation' },
        { name: 'Animals', link: 'http://localhost:3000/tag/3de8c75d-8ee3-48ff-98ee-e20a65c86451/animals' },
        { name: 'Comedy', link: 'http://localhost:3000/tag/4d32cc48-9f00-4cca-9b5a-a839f0764984/comedy' },
        { name: 'Adventure', link: 'http://localhost:3000/tag/87cc87cd-a395-47af-b27a-93258283bbc6/adventure' },
        { name: 'Magic', link: 'http://localhost:3000/tag/a1f53773-c69a-4ce5-8cab-fffcd90b1565/magic' },
        { name: 'Isekai', link: 'http://localhost:3000/tag/ace04997-f6bd-436e-b261-779182193d3d/isekai' },
        { name: 'Adaptation', link: 'http://localhost:3000/tag/f4122d1c-3b44-44d0-9936-ff7502c39ad3/adaptation' },
      ],
      coverUrl: "http://localhost:3000/cdn/covers/d4c40e73-251a-4bcb-a5a6-1edeec1e00e7/072834ae-5183-4ca8-bb25-ec597c340983.jpg.512.jpg",
      statistics: {
        comments: {
          total: 456,
          forumUrl: 'http://localhost:3000/forum/threads/123'
        },
        follows: 123,
        rating: {
          average: 7.5,
          bayesian: 7.5,
          votes: {
            "1": 1,
            "2": 2,
            "3": 3,
            "4": 4,
            "5": 5,
            "6": 6,
            "7": 7,
            "8": 8,
            "9": 9,
            "10": 10
          }
        }
      }
    } satisfies TitleModelData)
  })

  it('should throw an error when the id is not present in the statistics', async () => {
    vi.spyOn(Manga, 'getById').mockResolvedValue([{ data: mangaEntity, response: 'entity', result: 'ok' }, null])
    vi.spyOn(Manga, 'getStats').mockResolvedValue([{ result: 'ok', statistics: {} }, null])
    vi.spyOn(UserManager.prototype, 'getUser').mockResolvedValue({ access_token: undefined } as any)

    const model = new TitleModel(context, auth)
    await model.fetch('1234')

    const processor = () => model.process('en')
    expect(processor).toThrowError('Statistics are not fetched for title d4c40e73-251a-4bcb-a5a6-1edeec1e00e7.')
  })

  it('should throw an error when trying to process a title without fetching it', () => {
    const model = new TitleModel(context, auth)
    const processor = () => model.process('en')
    expect(processor).toThrowError('Title is not fetched.')
  })
})
