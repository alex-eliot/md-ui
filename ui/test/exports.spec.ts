import { describe, expect, it } from "vitest"
import * as ui from ".."

describe("ui", () => {
  it("should provide all exports", () => {
    expect(ui).toBeDefined()
    expect(ui).toHaveProperty("Context")
    expect(ui).toHaveProperty("PathGenerator")
    expect(ui).toHaveProperty("AuthContext")
    expect(ui).toHaveProperty("Model")
    expect(ui).toHaveProperty("AsyncModel")
    expect(ui).toHaveProperty("decorators")
    expect(ui).toHaveProperty("TitleModel")
  })
})
