import { LanguageAcronym, Manga, MangaEntity, MangaStatistics, is } from "@mangadex/wrapper"
import { AuthContext, Context, PathGenerator } from "@/core"
import { AsyncModel, decorators } from "@/models"
import { getTranslation, getFallbackCoverUrl, getTitleCoverUrl } from "@/common"

type VoteNumber = "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" | "10"
type Votes = Record<VoteNumber, number>

export type TitleModelProps = [string]

export type TitleModelData = {
  title: string
  altTitle: string
  otherAltTitles: {
      language: LanguageAcronym;
      title: string;
  }[]
  description: string
  artists: string[]
  authors: string[]
  tags: {
    link: string,
    name: string
  }[]
  coverUrl: string
  statistics: {
    follows: number
    comments: {
      total: number
      forumUrl: string | null
    }
    rating: {
      votes: Votes,
      bayesian: number,
      average: number
    }
  } | null
}

export class TitleModel extends AsyncModel<TitleModelProps> {
  title: MangaEntity<"author" | "artist" | "cover_art"> | null = null
  stats: MangaStatistics | null = null
  
  constructor(public context: Context, public auth: AuthContext) {
    super(context, auth)
  }

  @decorators.isFetching
  async fetch(id: string) {
    const [titleResult, statsResult] = await Promise.all([
      Manga.getById(id, ['artist', 'author', 'cover_art'], await this.auth.getSessionToken()),
      Manga.getStats(id, await this.auth.getSessionToken())
    ])

    const [title, mangaError] = titleResult
    const [stats, statsError] = statsResult

    this.title = title?.data ?? null
    this.stats = stats?.statistics ?? null
    this.error = mangaError ?? statsError
  }

  process(language: LanguageAcronym): TitleModelData {
    return {
      ...this.getTitleMetadata(language),
      coverUrl: this.getCoverUrl(),
      statistics: this.getStatistics()
    }
  }

  private getTitleMetadata(language: LanguageAcronym) {
    if (!this.title)
      throw new Error("Title is not fetched.")

    const pathGenerator = new PathGenerator(this.context)

    const description = getTranslation(language, this.title.attributes.description)
    const artists = this.title.relationships.filter(is("artist")).map(a => a.attributes.name)
    const authors = this.title.relationships.filter(is("author")).map(a => a.attributes.name)
    const tags = this.title.attributes.tags.map(t => ({
      link: pathGenerator.tag(t.id, getTranslation(language, t.attributes.name)),
      name: getTranslation(language, t.attributes.name)
    }))

    return {
      ...TitleModel.getMangaTitlesForLanguage(this.title, language),
      description,
      artists,
      authors,
      tags
    }
  }

  private getStatistics() {
    if (!this.title)
      throw new Error("Title is not fetched.")

    if (!this.stats || !(this.title.id in this.stats))
      throw new Error(`Statistics are not fetched for title ${this.title.id}.`)

    const pathGenerator = new PathGenerator(this.context)

    const mangaStats = this.stats[this.title.id]
    const averageRating = mangaStats.rating.average ?? 0
    const bayesianRating = mangaStats.rating.bayesian ?? 0
    const votes = Object.fromEntries(
      mangaStats.rating.distribution.map((v, i) => [`${i + 1}`, v])
    ) as Votes

    return {
      follows: mangaStats.follows,
      comments: mangaStats.comments
        ? {
          total: mangaStats.comments.repliesCount ?? 0,
          forumUrl: pathGenerator.forumThread(mangaStats.comments.threadId)
        }
        : {
          total: 0,
          forumUrl: null
        },
      rating: {
        votes,
        bayesian: bayesianRating,
        average: averageRating
      }
    }
  }

  private getCoverUrl() {
    if (!this.title)
      throw new Error("Title is not fetched.")

    const coverRelationship = this.title.relationships.find(is("cover_art"))
    const coverUrl = coverRelationship
      ? getTitleCoverUrl(this.context, this.title.id, coverRelationship, '512')
      : getFallbackCoverUrl(this.context)

    return coverUrl
  }

  static getMangaTitlesForLanguage(manga: MangaEntity<any>, language: LanguageAcronym) {
    const title = getTranslation(language, manga.attributes.title)
    const altTitle = getTranslation(language, manga.attributes.altTitles[0])
    const otherAltTitles = manga.attributes.altTitles.slice(1).flatMap(t => Object.entries(t).map(([k, v]) => ({
      language: k as LanguageAcronym,
      title: v
    })))
  
    return {
      title,
      altTitle,
      otherAltTitles
    }
  }
}
