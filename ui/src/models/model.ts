import { AuthContext, Context } from "@/core";
import { ErrorResult } from "@mangadex/wrapper";

export class Model {
  constructor(public context: Context) {}
}

export class AsyncModel<T extends any[]> {
  isFetching: boolean = false
  error: ErrorResult | null = null

  constructor(public context: Context, public auth: AuthContext) {}

  async fetch(...args: T) {
    console.log(...args)
  }
}

export const decorators = {
  /**
   * Sets the isFetching attribute to true while the decorated function is running
   */
  isFetching: (_: any, __: any, descriptor: any) => {
    const original = descriptor.value
    descriptor.value = async function (...args: any[]) {
      this.isFetching = true
      try {
        await original.apply(this, args)
      } catch (e) {
        throw e
      } finally {
        this.isFetching = false
      }
    }
  }
}
