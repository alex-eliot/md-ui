import { paramsParser } from "@mangadex/wrapper"
import { Context } from "@/core";
import urlConfig from "@/url.config";
import { kebab } from "..";

export class PathGenerator {
  constructor(private context: Context) {}

  private url(path: string, params: Record<string, any> = {}) {
    const queryString = paramsParser(params)
    return `${this.context.baseUrl}${path}${queryString ? `?${queryString}` : ""}`
  }

  private forumUrl(path: string, params: Record<string, any> = {}) {
    const queryString = paramsParser(params)
    return `${this.context.forumUrl}${path}${queryString ? `?${queryString}` : ""}`
  }

  tag(id: string, name: string) {
    return this.url(urlConfig.tag + `/${id}/${kebab(name)}`)
  }

  forumThread(threadId: string | number) {
    return this.forumUrl(urlConfig.forum.thread + `/${threadId}`)
  }

  oAuthCallback() {
    return this.url(urlConfig.oAuth.callback)
  }
}
