import { UserManager, WebStorageStateStore } from "oidc-client-ts"
import { Context, PathGenerator } from "@/core"

export class AuthContext {
  private userManager: UserManager

  constructor(context: Context, store?: WebStorageStateStore) {
    const pathGenerator = new PathGenerator(context)

    this.userManager = new UserManager({
      authority: `${context.oAuthUrl}/realms/mangadex`,
      client_id: context.oAuthClientId,
      redirect_uri: `${pathGenerator.oAuthCallback()}`,
      scope: "openid email groups profile roles",
      userStore: store ?? new WebStorageStateStore({ store: window.localStorage }),
      automaticSilentRenew: false,
    })
  }

  public async login(redirectBackTo?: string) {
    await this.userManager.signinRedirect({ redirect_uri: redirectBackTo })
  }

  public async logout(redirectBackTo?: string) {
    await this.userManager.signoutRedirect({ post_logout_redirect_uri: redirectBackTo })
  }

  public async getUser(){
    return await this.userManager.getUser()
  }

  public async getSessionToken() {
    const user = await this.getUser()
    return user?.access_token
  }

  public async completeLogin() {
    await this.userManager.signinRedirectCallback()
  }

  public async completeLogout() {
    await this.userManager.signoutRedirectCallback()
  }
}
