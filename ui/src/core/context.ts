/**
 * Creates a UI context that contains the current state of the application.
 * Information such as baseUrl, apiUrl, cdnUrl, and other configuration is contained here
 */
export class Context {
  /** The base URL of the application */
  baseUrl: string
  /** The base URL of the API */
  apiUrl: string
  /** The base URL of the CDN */
  cdnUrl: string
  /** The base URL of the forum */
  forumUrl: string
  /** The base URL of the OAuth server */
  oAuthUrl: string
  /** The client ID of the OAuth application */
  oAuthClientId: string

  constructor(options: {
    /** The base URL of the application */
    baseUrl: string
    /** The base URL of the API */
    apiUrl: string
    /** The base URL of the CDN */
    cdnUrl: string
    /** The base URL of the forum */
    forumUrl: string
    /** The base URL of the OAuth server */
    oAuthUrl: string
    /** The client ID of the OAuth application */
    oAuthClientId: string
  }) {
    this.apiUrl = options.apiUrl
    this.baseUrl = options.baseUrl
    this.cdnUrl = options.cdnUrl
    this.forumUrl = options.forumUrl
    this.oAuthUrl = options.oAuthUrl
    this.oAuthClientId = options.oAuthClientId
  }
}
