import util from "util"

export enum LogLevel {
  LOG = 'LOG',
  ERROR = 'ERROR',
  WARN = 'WARN',
  INFO = 'INFO',
  DEBUG = 'DEBUG'
}

const loggerStyles: Record<string, string> = {
  "test": "color: #ff00ff",
}

export class Logger {
  records: string[] = []

  constructor(
    /** The name of the logger */
    public name: string,
    /** The log levels to emit to the console */
    public emitLevels: LogLevel[] = []
  ) {}

  record(logLevel: LogLevel, ...args: any[]) {
    const message = `[${Logger.getCurrentDateString()} - loglevel:${logLevel}] ${util.format(...args)}`
    this.records.push(message)

    if (this.emitLevels.includes(logLevel))
      this.logToConsole(message, logLevel)
  }

  log(...args: any[]) {
    this.record(LogLevel.LOG, ...args)
  }

  error(...args: any[]) {
    this.record(LogLevel.ERROR, ...args)
  }

  warn(...args: any[]) {
    this.record(LogLevel.WARN, ...args)
  }

  info(...args: any[]) {
    this.record(LogLevel.INFO, ...args)
  }

  debug(...args: any[]) {
    this.record(LogLevel.DEBUG, ...args)
  }

  clear() {
    this.records.splice(0, this.records.length)
  }

  getRecords(...levels: LogLevel[]) {
    return this.records.filter(record => recordHasAnyLogLevel(record, ...levels))
  }

  static getCurrentDateString() {
    return new Date().toISOString()
  }

  logToConsole(message: string, logLevel: LogLevel) {
    const method = logLevel.toLowerCase() as 'log' | 'error' | 'warn' | 'info' | 'debug'
    return console[method](`%c ${message}`, loggerStyles[this.name] || "")
  }
}

export class LogTracker {
  loggers: Record<string, Logger> = {}

  constructor() {}

  getLogger(name: string, emitLevels: LogLevel[] = []) {
    if (!this.loggers[name])
      this.loggers[name] = new Logger(name, emitLevels)

    return this.loggers[name]
  }

  dumpLogs(...levels: LogLevel[]) {
    const records = this.getAllLogRecordsForLevels(...levels)
  
    records.sort((a, b) => {
      const dateA = new Date(a.slice(1, 25))
      const dateB = new Date(b.slice(1, 25))
  
      if (dateA < dateB) return -1
      if (dateA > dateB) return 1
      return 0
    })
  
    return records
  }

  getAllLogRecordsForLevels(...levels: LogLevel[]) {
    return Object
      .values(this.loggers)
      .flatMap(logger => logger.getRecords(...levels))
  }
}

function recordHasAnyLogLevel(record: string, ...levels: LogLevel[]) {
  if (levels.length === 0) return false
  const logLevelRegex = new RegExp(`loglevel:(${levels.join('|')})`);
  return logLevelRegex.test(record)
}
