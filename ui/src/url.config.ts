export default {
  tag: "/tag",
  forum: {
    thread: '/threads'
  },
  oAuth: {
    callback: '/auth/login'
  }
} as const
