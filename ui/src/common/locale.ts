import { LanguageAcronym, LocalizedString } from "@mangadex/wrapper"
import { typedKeys } from "@/common"

/**
 * Get the translation for the given language.
 * If the translation for the given language is not available,
 * it will try to get the translation for the default language (en),
 * and if that is not available either, it will return the first translation available.
 * @param language the language to get the translation for
 * @param coded the translations object
 * @returns the translation for the given language
 */
export function getTranslation(language: LanguageAcronym, coded: LocalizedString) {
  return coded[language] || coded.en || coded[typedKeys(coded)[0]] || ""
}
