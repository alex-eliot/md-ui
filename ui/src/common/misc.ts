export function typedKeys<T extends object>(obj: T) {
  return Object.keys(obj) as (keyof T)[];
}

/**
 * Converts a string into a readable url slug
 *
 * @param text string to be converted into kebab
 * @param limit the maximum amount of characters allowed in the kebab, truncates by whole words, -1 results in no limit
 */
export function kebab (text?: string | null, limit = 100) {
  if (!text) return "";
  const kebab = text
    .trim()
    .replace(/ *\([^)]*\) */g, "") // remove parenthesized text
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .toLowerCase()
    .replace(/[^a-z\d]+/g, "-")
    .replace(/-+$/, "");
  if (limit < 0) return kebab;

  const wordList = kebab.split("-");

  let result: string = "";
  for (const word of wordList) {
    if (result.length + word.length > limit) break;
    else result += word + "-";
  }

  return result.substring(0, result.length - 1);
};
