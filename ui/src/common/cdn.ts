import { Context } from "@/core";
import { CoverEntity, CoverRelationship, ExpandedRelationship, is } from "@mangadex/wrapper";

export type CoverSize = "256" | "512" | "original"

export type CoverUrlParamsEntity = [
  context: Context,
  cover: CoverEntity,
  size?: CoverSize
]

export type CoverUrlParamsRelation = [
  context: Context,
  titleId: string,
  cover: ExpandedRelationship<CoverRelationship>,
  size?: CoverSize
]

export type CoverUrlParams = CoverUrlParamsEntity | CoverUrlParamsRelation

export function getTitleCoverUrl(...args: CoverUrlParamsEntity): string
export function getTitleCoverUrl(...args: CoverUrlParamsRelation): string
export function getTitleCoverUrl(...args: CoverUrlParams) {
  switch (args.length) {
    case 3: return getTitleCoverByCoverEntity(...args as CoverUrlParamsEntity)
    case 4: return getTitleCoverByCoverRelation(...args as CoverUrlParamsRelation)
  }
}

function getTitleCoverByCoverEntity(context: Context, cover: CoverEntity, size?: CoverSize) {
  const titleId = cover.relationships.find(is("manga"))?.id
  const quality = size === "original" ? "" : `${size}.jpg`
  return `${context.cdnUrl}/covers/${titleId}/${cover.attributes.fileName}${quality}`
}

function getTitleCoverByCoverRelation(context: Context, titleId: string, cover: ExpandedRelationship<CoverRelationship>, size?: CoverSize) {
  const quality = size === "original" ? "" : `.${size}.jpg`
  return `${context.cdnUrl}/covers/${titleId}/${cover.attributes.fileName}${quality}`
}

export function getFallbackCoverUrl(context: Context) {
  return `${context.cdnUrl}/cover-placeholder.jpg`
}
